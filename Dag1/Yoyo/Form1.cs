﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yoyo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Calc1();
        }

        private void Calc1()
        {
            List<int> vals = ValuesFromTextbox();

            for (int i = 0; i < vals.Count; i++)
            for(int j=0; j<vals.Count; j++)
                    for(int k=0; k<vals.Count; k++)
                {
                    if (i != j && j!=k && vals[i]+vals[j]+vals[k] == 2020)
                    {
                        Console.WriteLine("Answer: " + vals[i] * vals[j] * vals[k]);
                    }
                }
        }
        private List<int> ValuesFromTextbox()
        {
            char[] splittos = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(splittos, StringSplitOptions.RemoveEmptyEntries);
            List<int> result = new List<int>();
            foreach(string s in sp)
            {
                result.Add(int.Parse(s));
            }
            return result;
        }

    }
}
