﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag18
{
    public abstract class Node
    {
        public abstract long Eval();
    }

    public class NodeNumber : Node
    {
        private long number;

        public NodeNumber(long thelong)
        {
            number = thelong;
        }

        public override long Eval()
        {
            return number;
        }
    }

    public class NodeBinary : Node
    {
        public NodeBinary(Node lhs, Node rhs, Func<long, long, long> op)
        {
            _lhs = lhs;
            _rhs = rhs;
            _op = op;
        }

        Node _lhs;
        Node _rhs;
        Func<long, long, long> _op;

        public override long Eval()
        {
            long lhsVal = _lhs.Eval();
            long rhsVal = _rhs.Eval();

            long res = _op(lhsVal, rhsVal);
            return res;
        }
    }

    public enum Token
    {
        EOF,
        ADD,
        SUB,
        MULT,
        DIV,
        OPENPAR,
        CLOSEPAR,
        NUMBER
    }

    public class Tokenizer
    {
        TextReader _reader;
        public Token Token;
        public long Number;

        public Tokenizer(TextReader reader)
        {
            _reader = reader;
            NextChar();
            NextToken();
        }

        char _currentChar;


        void NextChar()
        {
            long ch = _reader.Read();
            _currentChar = ch < 0 ? '\0' : (char)ch;
        }

        public void NextToken()
        {
            // Skip whitespace
            while (char.IsWhiteSpace(_currentChar))
                NextChar();

            switch(_currentChar)
            {
                case '\0':
                    Token = Token.EOF;
                    return;

                case '+':
                    NextChar();
                    Token = Token.ADD;
                    return;
                case '-':
                    NextChar();
                    Token = Token.SUB;
                    return;
                case '*':
                    NextChar();
                    Token = Token.MULT;
                    return;
                case '(':
                    NextChar();
                    Token = Token.OPENPAR;
                    return;
                case ')':
                    NextChar();
                    Token = Token.CLOSEPAR;
                    return;
            }

            if (char.IsDigit(_currentChar))
            {
                StringBuilder sb = new StringBuilder();
                while (char.IsDigit(_currentChar))
                {
                    sb.Append(_currentChar);
                    NextChar();
                }

                this.Number = long.Parse(sb.ToString());
                Token = Token.NUMBER;
                return;
            }

            throw new InvalidDataException("What is this char " + _currentChar);
        }
    }


    public class Parser
    {
        Tokenizer _tokenizer;
        public Parser(Tokenizer tok)
        {
            _tokenizer = tok;
        }

        public Node ParseExpression()
        {
            // Oh, we should be finish

            Node res = ParseMultDiv();

            if (_tokenizer.Token != Token.EOF)
                throw new SyntaxErrorException("Error error");

            return res;
            
        }

        Node ParseAddSub()
        {
            // +-*/ har samma precendce i uppgift
            Node lhs = ParseLeaf();

            while (true)
            {
                Func<long, long, long> op = null;
                if (_tokenizer.Token == Token.ADD)
                {
                    op = (a, b) => a + b;
                }
                else if (_tokenizer.Token == Token.SUB)
                {
                    op = (a, b) => a - b;
                }
                
                if (op == null)
                    return lhs;

                _tokenizer.NextToken();
                Node rhs = ParseLeaf();

                // Create binary node and use it as the left-hande side
                lhs = new NodeBinary(lhs, rhs, op);

            }
        }

        Node ParseMultDiv()
        {
            // +-*/ har samma precendce i uppgift
            Node lhs = ParseAddSub();

            while(true)
            {
                Func<long, long, long> op = null;
                if (_tokenizer.Token == Token.MULT)
                {
                    op = (a, b) => a * b;
                }
                else if (_tokenizer.Token == Token.DIV)
                {
                    op = (a, b) => a / b;
                }

                if (op == null)
                    return lhs;

                _tokenizer.NextToken();
                Node rhs = ParseAddSub();

                // Create binary node and use it as the left-hande side
                lhs = new NodeBinary(lhs, rhs, op);

            }
        }

        Node ParseLeaf()
        {
            if (_tokenizer.Token == Token.NUMBER)
            {
                Node node = new NodeNumber(_tokenizer.Number);
                _tokenizer.NextToken();
                return node;
            }

            if (_tokenizer.Token == Token.OPENPAR)
            {
                // Skip '('
                _tokenizer.NextToken();
                Node node = ParseMultDiv();

                // Check and skip ')'
                if (_tokenizer.Token != Token.CLOSEPAR)
                    throw new SyntaxErrorException("Missing close parenthesis gah");
                _tokenizer.NextToken();

                return node;
            }

            throw new SyntaxErrorException("Aaargh.. wuzz. .lägg till stöd");
        }
    }


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //Node cool = new Parser(new Tokenizer(new StringReader(textBox1.Text))).ParseExpression();
            //Console.WriteLine("Wutt " + cool.Eval());

            List<long> summit = new List<long>();
            foreach(string s in GetInput())
            {
                long res = new Parser(new Tokenizer(new StringReader(s))).ParseExpression().Eval();
                Console.WriteLine(s + " = " + res);
                summit.Add(res);
            }

            Console.WriteLine("Ohhh " + summit.Sum());

        }

        List<string> GetInput()
        {
            // Rader me stuff
            char[] seppos = { '\r', '\n' };
            return textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
