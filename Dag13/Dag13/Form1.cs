﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //long timestamp = GetTimeStamp();
            List<int> busIds = GetBusIds();

            //long bestWait = long.MaxValue;
            //int bestID = 0;
            //foreach(int bus in busIds)
            //{
            //    long wt = WaitTime(timestamp, bus);
            //    if (wt < bestWait)
            //    {
            //        bestWait = wt;
            //        bestID = bus;
            //    }
            //}

            GetAdvancedWait(textBox1.Text, busIds);

            //   Console.WriteLine("Wohoo " + bestWait* bestID);

        }


        long LCD(List<int> offsets, List<int> busids)
        {
            // Aha.. do we have this cool relationship?

            return 409 * 29;
            List<long> multisar = new List<long>();

            for (int i = 0; i < busids.Count; i++)
                for (int j = 0; j < busids.Count; j++)
                {
                    if (!(j < multisar.Count))
                    {
                        multisar.Add(1);
                    }
                    if (i != j)
                        multisar[i] *= busids[j];
                }


            // Oh.. all primes
            return multisar.Sum();
            long mus = 1;
            foreach (int v in busids)
                mus *= v;
            return mus;
        }

        long GetAdvancedWait(string bbbs, List<int> busids)
        {
            List<int> offsets = new List<int>();
            int i = 0;
            char[] seppos = { '\n', '\r', ',' };
            string[] off = bbbs.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            foreach (string c in off)
            {
                if (c != "x")
                    offsets.Add(i);
                i++;
            }

            long time = 0;
            long addTime = LCD(offsets, busids);
            int solvIndex = 1;
            addTime = busids[0];
            while (true)
            {
                // Multiples of first or better ??                
                time += addTime;
                int solvOffset = offsets[solvIndex];
                if ((time + solvOffset) % busids[solvIndex] == 0)
                {
                    // Wow.. go on to the next bus
                    solvIndex++;

                    if (solvIndex == busids.Count)
                    {
                        // done
                        Console.WriteLine("Yea : " + time);
                        return time;
                    }
                    else
                    {
                        // Guess we need to fix addtime, 
                        addTime *= busids[solvIndex - 1];
                    }
                }


                //bool allGoodStuff = true;
                //for (i = 1; i < offsets.Count; i++)
                //{
                //    // Galet med negativt värde right.. tok                        
                //    int m = 1;
                //    while (busids[i] < offsets[i])
                //    {
                //        while (busids[i] * m < offsets[i])
                //            m++;
                //        busids[i] *= m;
                //    }
                //    int aim = busids[i] - offsets[i];
                //    if (time % busids[i] != aim)
                //    {
                //        allGoodStuff = false;
                //        break;
                //    }

                //}
                //if (allGoodStuff)
                //{
                //    Console.WriteLine("Yea : " + time);
                //    return time;
                //}
            }
        }
        
    

        long WaitTime(long timestamp, int busid)
        {
            long startIter = timestamp / busid;
            long tmp;
            while(true)
            {
                tmp = startIter * busid;
                if (tmp >= timestamp)
                {
                    return tmp - timestamp;
                }
                startIter++;
            }
        }

        long GetTimeStamp()
        {
            char[] seppos = { '\n', '\r' };
            return long.Parse(textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries)[0]);
        }
        List<int> GetBusIds()
        {
            char[] seppos = { '\n', '\r',',' };
            List<int> res = new List<int>();
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            bool skip = false; // part 2 false
            foreach (string s in sp)
            {
                if (!skip)
                    if (s[0] != 'x')
                        res.Add(int.Parse(s));
                skip = false;
            }
            return res;
        }


    }
}
