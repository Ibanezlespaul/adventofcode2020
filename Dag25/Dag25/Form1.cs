﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag25
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Test
            int t1 = Encrypt(8, 17807724);
            if (t1 != 14897079)
                Console.WriteLine("Encryption sucks");

            char[] sepps = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(sepps, StringSplitOptions.RemoveEmptyEntries);
            int cardPublic = int.Parse(sp[0]);
            int doorPublic = int.Parse(sp[1]);

            int cardLoopSize = GetLoop(cardPublic);
            int doorLoopSize = GetLoop(doorPublic);

            int enc1 = Encrypt(cardLoopSize, doorPublic);
            int enc2 = Encrypt(doorLoopSize, cardPublic);
        }
        int GetLoop(int publicKey)
        {
            int loops = 1;
            long value = 1;
            while(true)
            {
                value *= 7;
                value = value % 20201227;
                if (value == publicKey)
                    return loops;
                loops++;
            }            
        }

        int Encrypt(int loopSize, int publicKey)
        {
            long value = 1;
            while(loopSize > 0)
            {
                value *= publicKey;
                value = value % 20201227;
                loopSize--;
            }
            return (int)value;
        }
    }
}
