﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag10
{
    //public static class Ex
    //{
    //    public static IEnumerable<IEnumerable<T>> DifferentCombinations<T>(this IEnumerable<T> elements, int k)
    //    {
    //        return k == 0 ? new[] { new T[0] } :
    //          elements.SelectMany((e, i) =>
    //            elements.Skip(i + 1).DifferentCombinations(k - 1).Select(c => (new[] { e }).Concat(c)));
    //    }
    //}

    public partial class Form1 : Form
    {
        Dictionary<string, bool> seenThis = new Dictionary<string, bool>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<int> adapters = GetInput();
            adapters.Add(0);
            adapters.Sort();
            //int ones=0, threes=0;
            
            //for(int i=0; i<adapters.Count-1; i++)
            //{
            //    if (adapters[i + 1] - adapters[i] == 1)
            //        ones++;
            //    else if (adapters[i + 1] - adapters[i] == 3)
            //        threes++;

            //}
            //threes++;

            //Console.WriteLine(ones * threes);



            // Oh.. 

//            long variations = DoStuff(adapters,adapters.Max()+3);
        List<List<int>> segs = GetSegments(adapters);

            long mult = 1;
            foreach(List<int> s in segs)
            {
                mult *= DoStuff(s, s.Max() + 3, s.Min() - 3);
            }

            //Console.WriteLine("Variations: " + variations);
        }

        List<List<int>> GetSegments(List<int> input)
        {
            List<List<int>> res = new List<List<int>>();
            // Starter
            int start = 0;

            int index = 0;
            
            while (true)
            {
                List<int> next = new List<int>();
                for (int i = index; i < input.Count; i++)
                {
                    if (i == start)
                    {
                        next.Add(input[i]);
                        index++;
                    }
                    else if (input[i] - input[i - 1] == 1)
                        next.Add(input[i]);
                    else
                    {
                        // yea.. done with this segment
                        res.Add(next);
                        index = i;
                        start = index;
                        next = new List<int>();
                        break;
                    }
                }
                if (index >= input.Count)
                {
                    // Oh Oops.. 
                    if (next.Count>0)
                        res.Add(next);
                    break;
                }
            }

            return res;
        }


        bool Tested(List<int> adapters)
        {
            string ape = "";
            foreach (int i in adapters)
                ape += i + " ";
            if (!seenThis.ContainsKey(ape))
            {
                seenThis.Add(ape, true);
                return false;
            }
            return true;
        }

        long DoStuff(List<int> adapters, int high, int low)
        {
            long sum = 0;

            if (Tested(adapters) == false && GoodOne(adapters, high, low))
            {
                
                    sum++;
            }
            else
                return sum;

            if (adapters.Count>0)
            {
                for (int i=0; i<adapters.Count; i++)
                {
                    int cand = adapters[i];
                    List<int> smallerOne = adapters.ToList();
                    smallerOne.Remove(cand);
                    sum += DoStuff(smallerOne, high, low);
                }
            }
            return sum;
        }


        bool GoodOne(List<int> adapters, int high, int low)
        {
            adapters.Sort();
            for(int i=0;i<adapters.Count; i++)
            {
                if (i == 0)
                    if (adapters[0]-low > 3)
                        return false;
                if (i == adapters.Count - 1)
                    if (adapters[i] + 3 < high)
                        return false;
                // Check interconnections
                if (i > 0)
                    if (adapters[i] - adapters[i - 1] > 3)
                        return false;
            }
            if (adapters.Count == 0)
                return false;
            return true;
        }

        int GetVariations(List<int> adapters, int high)
        {
            if (adapters.Count == 0)
            {
                if (high < 4)
                    return 1;
                else
                    return 0;
            }
            else
            {
                // Oh.. få med lite variationer här
                int sum = 0;
                for (int i=0; i<adapters.Count; i++)
                {
                    if (high - adapters[i] < 4)
                    {
                        // A valid candidate .. Add 
                        Console.WriteLine("Candidate : " + adapters[i]);
                        List<int> smallerOne = adapters.ToList();
                        // Remove candidate
                        int removed = adapters[i];
                        smallerOne.Remove(adapters[i]);
                        smallerOne.Sort();
                        // Om borttagen mindre än max.. behåll gamla max
                        int nextHigh = removed;
                        if (smallerOne.Count > 0 && removed < smallerOne.Max())
                            nextHigh = high;
                        sum += GetVariations(smallerOne, nextHigh);
                    }
                }
                return sum;
            }
        }

        void Arr2(List<int> adapters)
        {            
            int high = adapters.Max() + 3;
            Console.WriteLine("Variations : " + GetVariations(adapters, high));

            //List<int> candidates = GetCandidates(adapters, high);
            //foreach(int c in candidates)
            //{

            //}

        }
           

        //void Arrangements(List<int> adapters)
        //{
        //    int valid = 0;
        //    int highPlus3 = adapters.Max() + 3;
        //    for (int k = adapters.Count; k > 1; k--)
        //    {
        //        var c = Ex.DifferentCombinations(adapters, k);
        //        // 0 must be in this one
        //        foreach (IEnumerable<int> l in c)
        //        {
        //            List<int> lst = l.ToList<int>();
        //            lst.Add(0);
        //            lst.Add(highPlus3);
        //            lst.Sort();
        //            bool ok = true;
        //            for (int i = 0; i < lst.Count - 1; i++)
        //            {
        //                int diff = lst[i + 1] - lst[i];
        //                if (diff < 1 || diff > 3)
        //                {
        //                    ok = false;
        //                    break;
        //                }
        //            }
        //            valid += ok ? 1 : 0;
        //        }
        //    }

        //    Console.WriteLine("Cowa: " + valid);
        //}

        List<int> GetInput()
        {
            char[] seppos = { '\n', '\r' };
            List<int> res = new List<int>();
            foreach (string s in textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries))
                res.Add(int.Parse(s));
            return res;
        }
    }
}
