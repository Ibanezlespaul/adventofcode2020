﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int total = 0;
            foreach(string line in Getlines())
            {
                char[] sepp = { ' ', ':' };
                string[] s = line.Split(sepp, StringSplitOptions.RemoveEmptyEntries);
                char[] seppo = { '-' };
                string[] range = s[0].Split(seppo);
                int low = int.Parse(range[0]);
                int high = int.Parse(range[1]);

                char find = s[1][0];

                int found = 0;
                // Policy 1
                //foreach (char c in s[2])
                //{
                //    if (c == find)
                //        found++;
                //}

                // OK NOK?
                //if (found >= low && found <= high)
                //    total++;

                // Policy 2
                if (s[2][low - 1] == find ^ s[2][high - 1] == find)
                    total++;
            }

            Console.WriteLine("Yeye: " + total);

        }

        string[] Getlines()
        {
            char[] seppos = { '\r', '\n' };
            return textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
