﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag23
{
    public class Cup
    {
        public int label;
        public Cup before;
        public Cup after;
    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Felsöker via del 1

            // Another crabgame!! :/
            int cupMax = 0;
            Cup first = GetInput(ref cupMax);
            // Hörru cupApa
            Cup last = first;
            while (last.after != null)
                last = last.after;


            // Gahhh.
            // Bah.. build on input
            Dictionary<int, Cup> dic = new Dictionary<int, Cup>();

            // Del 2 stuff

            int left = 1000000 - cupMax;
            for (int i = 0; i < left; i++)
            {
                Cup nyJävel = new Cup();
                last.after = nyJävel;
                nyJävel.label = i + cupMax + 1;
                nyJävel.before = last;
                last = nyJävel;
            }

            // Vafan.. bra att ha i en dictionary
            Cup allCupps = first;
            for (int i=0; i<1000000; i++)
            {
                dic[allCupps.label] = allCupps;
                allCupps = allCupps.after;
            }

            // Oh.. connect the circle
            first.before = last;
            last.after = first;

            bool once = true;

            int simulations = 10000000;
            Cup current = first;
            while (simulations>0)
            {
                //      Console.WriteLine("Current cup " + cups[current]);

                Cup firstPooped;
                Cup lastPooped;
                firstPooped = current.after;
                lastPooped = firstPooped.after.after; // Ha? =) 
                // Connect current with lastpopped.after
                current.after = lastPooped.after;
                firstPooped.before = null; // LIsta ut sen
                lastPooped.after = null; // yeye. måste listas ut sa jag

                // Find target
                int targetLabel = current.label - 1;
                // Oh.. finns denna bland de vi ploppat bort ligger vi pyrt till
                Cup destination = null ;
                while(true)
                {


                    if (targetLabel == firstPooped.label || targetLabel == firstPooped.after.label || targetLabel == firstPooped.after.after.label)
                    {
                        // Minska med en o sök, wut.. hur kan jag snabba upp det här va? öh.. alltså.. jooo.. lägg in dem i en jävla dictionary
                        
                    }
                    else
                    {
                        // Dictionary?
                        if (dic.ContainsKey(targetLabel))
                        {
                            // Wow
                            destination = dic[targetLabel];
                            break;
                        }
                    }
                    targetLabel--;
                    if (targetLabel < 0)
                        targetLabel = 1000000;
                }

                // Ok. .here we wanna put them
                Cup afterDest = destination.after;
                destination.after = firstPooped;
                firstPooped.before = destination;
                lastPooped.after = afterDest;
                afterDest.before = lastPooped;

                current = current.after;
                simulations--;

               
                //if (once && simulations % 1000 == 0)
                //{
                //    once = false;
                //    Console.WriteLine("Yay " + simulations);
                //MessageBox.Show("Apans " + simulations);
                //}
               

            }

            // Collect them stuffis
            Console.WriteLine("Part1");
            Cup theOne = dic[1];
            for(int i=0; i<9; i++)
            {
                Console.Write(theOne.label);
                theOne = theOne.after;
            }
            Console.WriteLine("Part1 klar");


            string yesplz = "Hallå lr " + dic[1].after.label + " say what " + dic[1].after.after.label + " multo = " + (long)dic[1].after.label * (long)dic[1].after.after.label; 
            MessageBox.Show(yesplz);
            Console.WriteLine("");


        }

        Cup GetInput(ref int cupMax)
        {
            Cup first = new Cup();
            Cup before=null;
            for (int i = 0; i < textBox1.Text.Length; i++)
            {
                if (i == 0)
                {
                    first.label = int.Parse(textBox1.Text[0].ToString());
                    cupMax = first.label;
                    before = first;
                }
                else
                {
                    Cup next = new Cup();
                    next.label = int.Parse(textBox1.Text[i].ToString());
                    next.before = before;
                    before.after = next;
                    before = next;
                    cupMax = Math.Max(cupMax, next.label);
                }
            }

            return first;

        }

    }
}
