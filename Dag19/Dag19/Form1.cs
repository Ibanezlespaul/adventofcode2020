﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Dag19
{

    public class Rule
    {
        public bool isChar;
        public char theChar;
        public List<int> firstRules = new List<int>(); 
        public List<int> otherRules = new List<int>(); // After or statement
    }

    public partial class Form1 : Form
    {

        Dictionary<int, Rule> myDic = new Dictionary<int, Rule>();
        Dictionary<string, bool> allStuff = new Dictionary<string, bool>();


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> cases = GetInput();

            string reg = FillStrings();


            int sum = 0;
            foreach(string s in cases)
            {
                if (Regex.IsMatch(s, reg))
                    sum++;
            }
            Console.WriteLine("Wut");
        }


        string FillStrings()
        {
            Rule zero = myDic[0];
            StringBuilder s = new StringBuilder();
            BuildString(zero, s);

            string regexet = "^" + s + "$";
            return regexet;           
        }

        void BuildString(Rule r, StringBuilder sb)
        {
            // OH.. try to build a regex .. that would be coolish

            if (r.isChar)
            {
                sb.Append(r.theChar);               
            }
            else
            {
                // Oh.. this could be a "only first rules" or a combined
                List<string> first = new List<string>();
                List<string> others = new List<string>();

                // Do we have an OR?
                sb.Append("(");

                foreach (int i in r.firstRules)
                {
                    BuildString(myDic[i], sb);
                }
                // Do we have an and??
                if (r.otherRules.Count > 0)
                    sb.Append("|");

                foreach (int i in r.otherRules)
                    BuildString(myDic[i], sb);

                sb.Append(")");
            }
        }


        bool MatchString(string s)
        {
            Rule zero = myDic[0];

            for(int i=0; i<s.Length; i++)
            {
                bool ok = RuleOk(zero, i, s[i], 0);
                if (!ok)
                    return false;
            }
            return true;
        }

        bool RuleOk(Rule r, int charIndex, char c, int currentIndex)
        {
            if (r.isChar && charIndex != currentIndex)
                return false;
            if (r.isChar && charIndex == currentIndex)
                return r.theChar == c;


            bool rulesMatched = true;
            if (r.firstRules.Count > 0)
            {
                // Check first part
                for (int i = 0; i < r.firstRules.Count; i++)
                {
                    if (!RuleOk(myDic[r.firstRules[i]], charIndex, c, currentIndex+i))
                    {
                        rulesMatched = false;
                        break;
                    }
                }               
            }
            if (rulesMatched)
                return true;

            // Do we have a second chance??
            if (r.otherRules.Count > 0)
            {
                rulesMatched = true;
                for (int i = 0; i < r.otherRules.Count; i++)
                {
                    // match one of these
                    if (!RuleOk(myDic[r.otherRules[i]], charIndex, c, currentIndex + i))
                    {
                        rulesMatched = false;
                        break;
                    }
                }
            }

            return rulesMatched;

            throw new Exception("Should not go here right?");
        }

        List<string> GetInput()
        {
            myDic = new Dictionary<int, Rule>();

            char[] sepps = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(sepps, StringSplitOptions.RemoveEmptyEntries);
            List<string> testCases = new List<string>();
            foreach(string s in sp)
            {
                if (s.Contains(":"))
                {
                    char[] mellan = { ' ',':' };
                    string[] sr = s.Split(mellan, StringSplitOptions.RemoveEmptyEntries);

                    int key = int.Parse(sr[0]);
                    Rule r = new Rule();
                    bool first = true;

                    // Oh. might be the end-case.. a character
                    for (int i = 1; i < sr.Length; i++)
                    {
                        if (sr[i].Length > 1 && char.IsLetter(sr[i], 1))
                        {
                            r.theChar = sr[i][1];
                            r.isChar = true;
                            break;
                        }

                        if (sr[i] == "|")
                        {
                            first = false;
                            continue;
                        }
                        if (first)
                            r.firstRules.Add(int.Parse(sr[i]));
                        else
                            r.otherRules.Add(int.Parse(sr[i]));
                    }

                    myDic[key] = r;                   
                }
                else
                {
                    testCases.Add(s);
                }
            }
            return testCases;
        }


    }
}
