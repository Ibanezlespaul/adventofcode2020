﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[,] grid = GetGrid();
            PrintGrid(grid);
            
            while(ApplyRules(grid))
            {
                Console.WriteLine("");
                PrintGrid(grid);
                
            }
            int occ = GetOccupied(grid);
        }

        bool ApplyRules(char[,] grid)
        {
            // Did we change anything?
            char[,] before = grid.Clone() as char[,];
            bool changed = false;
            for (int y = 0; y < before.GetLength(1); y++)
            {
                for (int x = 0; x < before.GetLength(0); x++)
                {
                    // Rule 1
                    if (before[x, y] == 'L')
                    {
                        int occadj = GetOccAdjPart2(before, x, y);
                        if (occadj == 0)
                        {
                            grid[x, y] = '#';
                            changed = true;
                        }
                    }

                    // Rule 2
                    if (before[x, y] == '#')
                    {
                        int occadj = GetOccAdj(before, x, y);
                        //if (occadj >= 4)
                        //{
                        //    grid[x, y] = 'L';
                        //    changed = true;
                        //}
                        occadj = GetOccAdjPart2(before, x, y);
                        if (occadj >= 5)
                        {
                            grid[x, y] = 'L';
                            changed = true;
                        }
                    }
                }
            }
            return changed;
        }

        int GetOccupied(char[,] grid)
        {
            int res = 0;
            int width = grid.GetLength(0);
            int height = grid.GetLength(1);
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    res += grid[x, y] == '#' ? 1 : 0;
            return res;
        }

        int GetOccAdj(char[,] grid, int x, int y)
        {
            int width = grid.GetLength(0);
            int height = grid.GetLength(1);

            int occupied = 0;
            for (int xx=x-1; xx<x+2; xx++)
            {
                for (int yy=y-1;yy<y+2;yy++)
                {
                    if (!(x == xx && y==yy))
                    {
                        if (xx>=0 && xx<width && yy>=0 && yy<height)
                        {
                            occupied += grid[xx, yy] == '#' ? 1 : 0;
                        }
                    }
                }
            }
            return occupied;
        }
        int GetOccAdjPart2(char[,] grid, int x, int y)
        {
            // Shooot rayys kind of
            bool[] doneWithIt = new bool[8] { false, false, false, false, false, false, false, false };
            int width = grid.GetLength(0);
            int height = grid.GetLength(1);

            int occupied = 0;

            int xx = 0;
            int yy = 0;
            int maxStep = Math.Max(width, height);

            for (int step = 1; step < maxStep; step++)
            {
                for (int dir = 0; dir < 8; dir++)
                {
                    if (doneWithIt[dir] == false)
                    {
                        switch(dir)
                        {
                            case 0:
                                // Up
                                xx = x;
                                yy = y + step;
                                break;
                            case 1:
                                // Up right
                                xx = x + step;
                                yy = y + step;
                                break;
                            case 2:
                                // Right
                                xx = x + step;
                                yy = y ;
                                break;
                            case 3:
                                // Down right
                                xx = x + step;
                                yy = y - step;
                                break;
                            case 4:
                                // Down
                                xx = x;
                                yy = y - step;
                                break;
                            case 5:
                                // Down left
                                xx = x - step;
                                yy = y - step;
                                break;
                            case 6:
                                // left
                                xx = x - step;
                                yy = y;
                                break;
                            case 7:
                                // Up left
                                xx = x - step;
                                yy = y + step;
                                break;
                        }

                        if (!(x == xx && y == yy))
                        {
                            if (xx >= 0 && xx < width && yy >= 0 && yy < height)
                            {

                                if (grid[xx, yy] == '#')
                                {
                                    doneWithIt[dir] = true;
                                    occupied++;
                                }
                                if (grid[xx, yy] == 'L')
                                {
                                    doneWithIt[dir] = true;
                                }
                            }
                        }
                    }
                }
            }

            return occupied;
        }

        void PrintGrid(char[,] gridden)
        {
            for (int y = 0; y < gridden.GetLength(1); y++)
            {
                for (int x = 0; x < gridden.GetLength(0); x++)
                {
                    Console.Write(gridden[x, y]);
                }
                Console.WriteLine("");
            }
        }

        char[,] GetGrid()
        {
            char[,] res;
            char[] seppos = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);

            int theY = sp.Length;
            int theX = sp[0].Length;

            res = new char[theX,theY];
            for (int y=0; y<sp.Length; y++)
            {
                for (int x=0; x<sp[y].Length;x++)
                {
                    res[x, y] = sp[y][x];
                }
            }

            return res;
        }
    }
}
