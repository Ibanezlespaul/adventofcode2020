﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Dag20
{
    public class Tile
    {
        public int ID;
        public char[,] grid;
        public Tile(char[,] fet)
        {
            grid = fet;
        }

        List<string> LochNess = new List<string>() { "                  # ",
                                                     "#    ##    ##    ###",
                                                     " #  #  #  #  #  #   " };



        int HashTags()
        {
            int sum = 0;
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    sum += grid[x, y] == '#' ? 1 : 0;
                }
            }
            return sum;
        }
        int GotLockNess()
        {
            int sum = 0;

            for (int x = 0; x < grid.GetLength(0) - LochNess[0].Length; x++)
            {
                for (int y = 0; y < grid.GetLength(1) - LochNess.Count; y++)
                {
                    // Söker den hela vägen ut lr va?
                    bool match = true;
                    for (int ly = 0; ly < LochNess.Count; ly++)
                        for (int lx = 0; lx < LochNess[0].Length; lx++)
                        {
                            if (LochNess[ly][lx] == '#')
                            {
                                // Match underlajing image
                                if (grid[x + lx, y + ly] == '#')
                                {
                                    // Allt väl
                                }
                                else
                                {
                                    match = false;
                                    if (ly == 2 && lx == 16)
                                    {
                                        Console.WriteLine("Failed at " + lx + " " + ly);
                                        Console.WriteLine("Eye down x: " + x + " y: " + y);
                                        Print();
                                        Console.WriteLine("Paause");
                                    }
                                    break;
                                }
                            }
                        }

                    // Wow. oh.. we got a match?
                    if (match)
                    {
                        // Ohh
                        sum++;
                    }
                }
            }
            return sum;
        }

        public void Part2Stuff()
        {
            int monsters = 0;
            // Which rotation has monsters?
            for (int rot = 0; rot < 8; rot++)
            {
                monsters = 0;

                if (rot == 0)
                {
                    monsters = GotLockNess();
                    if (monsters > 0)
                    {
                        Console.WriteLine("Cowabunga");
                    }
                }
                else
                {
                    // Do cool rotations
                    RotRight();
                    if (rot == 4)
                        FlipX();
                    monsters = GotLockNess();
                    if (monsters > 0)
                    {
                        Console.WriteLine("Cowabunga");

                        // OOpsii..  Hej mr grid.. tell me how many # you have.. 
                        int ht = HashTags();
                        int answ = ht - 15 * monsters;
                        Console.WriteLine("Del 2: " + answ);

                    }

                }
            }
        }


        public Tile(int id, List<string> stuff)
        {
            ID = id;
            int x = stuff[0].Length;
            int y = stuff.Count;

            grid = new char[x, y];
            for (int xx = 0; xx < x; xx++)
                for (int yy = 0; yy < y; yy++)
                    grid[xx, yy] = stuff[yy][xx];
        }

   
        public char[,] GetWithoutBorder()
        {
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
          
            char[,] wb = new char[xMax - 2, yMax - 2];

            for (int x=0; x<xMax-2; x++)
                for(int y=0; y<yMax-2; y++)
                {
                    wb[x, y] = grid[x + 1, y + 1];
                }

            return wb;
        }

        public void Print()
        {
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
            for (int y=0; y<yMax; y++)
            {
                Console.WriteLine("");
                for (int x=0; x<xMax; x++)
                {
                    Console.Write(grid[x, y] + "");
                }
            }
            Console.WriteLine("");
        }
        public void RotRight()
        {
            // omg.. oh noo what todo
            char[,] org = (char[,])grid.Clone();
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
            for (int x=0;x<xMax; x++)
                for (int y=0; y<yMax; y++)
                {
                    grid[yMax-y-1, x] = org[x, y];
                    
                }
        }
        public void FlipX()
        {
            char[,] org = (char[,])grid.Clone();
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                {
                    grid[xMax-x-1, y] = org[x, y];
                }
        }

        public char Match(Tile other)
        {
            // Where does the other Tile match, To the left of this one (return L)
            // Right, (R), Above (A), Below(B), No match (N)

            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);

            // OK.. check above (upper (y=0) of this compared to the other)
            bool match = true;
            for(int x=0; x<xMax; x++)
            {
                if (this.grid[x, 0] != other.grid[x, yMax-1])
                    match = false;
            }
            if (match)
                return 'A';

            // Check below!!
            match = true;
            for (int x = 0; x < xMax; x++)
            {
                if (this.grid[x, yMax-1] != other.grid[x, 0])
                    match = false;
            }
            if (match)
                return 'B';

            // To the left
            match = true;
            for (int y = 0; y < yMax; y++)
            {
                if (this.grid[0, y] != other.grid[xMax-1, y])
                    match = false;
            }
            if (match)
                return 'L';

            // To the right
            match = true;
            for (int y = 0; y < yMax; y++)
            {
                if (this.grid[xMax-1, y] != other.grid[0, y])
                    match = false;
            }
            if (match)
                return 'R';

            return 'N';
        }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Tile> myTiles = GetInput();

            Tile first = myTiles[0];
            myTiles.RemoveAt(0);

            List<Tile> matched = new List<Tile>();
            matched.Add(first);
            int[,] matchGrid = new int[2000,2000];
            for(int x=0; x<2000; x++)
                for (int y=0; y<2000; y++)
                {
                    matchGrid[x, y] = 0;
                }


            // Oh.. put first id in the grid
            matchGrid[1000, 1000] = first.ID;

            bool test = true;

            while(myTiles.Count > 0)
            {
                for(int t =0; t<myTiles.Count; t++)
                {
                    Tile aTile = myTiles[t];

                    // Try to match this tile against already matched
                    foreach(Tile mt in matched)
                    {
                        bool gotMatch = false;
                        // Oh. .test all rotations flips
                        int matchedID = 0;
                        char where = 'X';
                        for (int rot = 0; rot < 8; rot++)
                        {                           
                            if (rot ==0)
                            {
                                where = mt.Match(aTile);
                            }
                            else
                            {
                                // Do cool rotations
                                aTile.RotRight();
                                if (rot == 4)
                                    aTile.FlipX();
                                
                                where = mt.Match(aTile);
                                if (aTile.ID == 1423 && where != 'N')
                                    Console.WriteLine("Aaargh");
                            }

                            //Console.WriteLine("Rotation " + rot);
                            //aTile.Print();

                            if (where != 'N')
                            {
                                matchedID = mt.ID;
                                gotMatch = true;
                                break;
                            }
                        }

                        if(gotMatch)
                        {
                            // Pluck this little bugger from mytiles to matched.. and remember how it matched
                            myTiles.RemoveAt(t);
                            matched.Add(aTile);

                            if (test)
                            {
                                Console.WriteLine("ID: " + aTile.ID);
                                aTile.Print();
                                test = false;
                            }
                            FillGrid(matchGrid, matchedID, aTile.ID, where);
                            t = 10000;
                            break;
                        }
                    }


                }
            }
            Console.WriteLine("Yeah");
            PrintGrid(matchGrid);



            // Holy crap.. fixa en bild o sök efter monster...
            // Each piece will be 8x8... seems to be a grid of 12x12 tiles
            // Ok.. felsök genom att ta ut hela bilden.. 10x10 


            char[,] image = new char[96, 96];
            //char[,] image = new char[120, 120];
            int tileCount = 0;
            int tileWidth = 8;
            for (int y = 0; y < matchGrid.GetLength(1); y++)
                for (int x = 0; x < matchGrid.GetLength(0); x++)                
                {
                    if (matchGrid[x, y] != 0)
                    {
                        int tx = tileCount % 12;
                        int ty = tileCount / 12;

                        // OH.. Find that bugger
                        foreach(Tile t in matched)
                        {
                            if (t.ID == matchGrid[x,y])
                            {
                                if (t.ID == 1607)
                                    Console.WriteLine("Checka xy");
                                // Oh.. copy some stuff from this one to that one
                                char[,] small = t.GetWithoutBorder();
                                for(int sx=0; sx< tileWidth; sx++)
                                    for(int sy=0; sy< tileWidth; sy++)
                                    {
                                        // Ohh.. wutt wutt get right offset in big one
                                        image[tx * tileWidth + sx, ty * tileWidth + sy] = small[sx, sy];
                                    }
                            }
                        }
                        tileCount++;
                    }                    
                }

            Tile leImage = new Tile(image);
            // Find monsters.. 
       //     leImage.Print();
            leImage.Part2Stuff();

        }



        void PrintGrid(int[,] grid)
        {
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
            for (int y = 0; y < yMax; y++)
            {
                Console.WriteLine("");
                for (int x = 0; x < xMax; x++)
                {
                    if (grid[x,y] != 0) 
                        Console.Write(grid[x, y] + "\t ");
                }
            }
        }

        void FillGrid(int[,] grid, int besideID, int newID, char direction)
        {
            int xMax = grid.GetLength(0);
            int yMax = grid.GetLength(1);
            for (int x = 0; x < xMax; x++)
                for (int y = 0; y < yMax; y++)
                {
                    if (grid[x, y] == besideID)
                    {
                        switch (direction)
                        {
                            case 'A':
                                if (grid[x, y - 1] != 0)
                                    Console.WriteLine("TRubbel");
                                grid[x, y - 1] = newID;
                                break;
                            case 'B':
                                if (grid[x, y + 1] != 0)
                                    Console.WriteLine("TRubbel");

                                grid[x, y + 1] = newID;
                                break;
                            case 'L':
                                if (grid[x-1, y] != 0)
                                    Console.WriteLine("TRubbel");

                                grid[x - 1, y] = newID;
                                break;
                            case 'R':
                                if (grid[x+1, y ] != 0)
                                    Console.WriteLine("TRubbel");

                                grid[x + 1, y] = newID;
                                break;
                            default:
                                throw new Exception("AAaaa");
                        }
                    }
                }
        }
        


        List<Tile> GetInput()
        {
            List<Tile> res = new List<Tile>();
            char[] sepps = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(sepps, StringSplitOptions.RemoveEmptyEntries);

            List<string> tugg = new List<string>();
            int tileID=-1;
            foreach(string s in sp)
            {
                if (s.StartsWith("Tile"))
                {
                    // Oh.. another one.. 
                    if (tugg.Count > 0)
                    {
                        res.Add(new Tile(tileID, tugg));
                        tugg.Clear();
                        tileID = -1;
                    }

                    string reggis = @"(\d+)";
                    string numb = Regex.Match(s, reggis).ToString(); // Funkis?
                    tileID = int.Parse(numb);
                }
                else
                {
                    tugg.Add(s);
                }
            }

            // Oh.. one unfinished or?
            res.Add(new Tile(tileID, tugg));
            return res;
        }
    }
}
