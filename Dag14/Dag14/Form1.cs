﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Dag14
{
    public struct Block
    {
       public string mask;
        public List<long> toAdress;
        public List<long> value;
    };

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dictionary<long, long> memory = new Dictionary<long, long>();
            List<Block> blocks = GetBlocks();


            foreach(Block b in blocks)
            {
                ApplyBlockPart2(b, ref memory);
            }
            long summis = memory.Values.Sum();
            Console.WriteLine("Cowabung " + summis);
        }


        void ApplyBlock(Block b, ref Dictionary<long, long> memory)
        {
            string mask = b.mask.Substring(7);
            for (int i = 0; i < b.value.Count; i++)
            {
                long theValue = b.value[i];
                long setMask = 0;
                long nollMask = 0;
                for (long j =0; j <mask.Length; j++)
                {
                    // Tanken är att llra sen
                    if (mask[(int)j] == '1')
                        setMask = setMask | (long)((long)1 << (int)(mask.Length-j-1));
                    else if (mask[(int)j] == '0')
                        nollMask = nollMask | (long)((long)1 << (int)(mask.Length - j - 1));
                }
                long tmp = theValue | setMask;
                tmp &= ~nollMask;
                memory[b.toAdress[i]] = tmp;
            }
        }

        void ApplyBlockPart2(Block b, ref Dictionary<long, long> memory)
        {
            // X=floating, 0 oförändrad, 1 =sätt till 1
            string mask = b.mask.Substring(7);
            int xs = 0;
            for (int i = 0; i < mask.Length; i++)
                xs += mask[i] == 'X' ? 1 : 0;

            


            // Använda XS för att skapa ersättningsbittar för xskruttet .. oh 

            for (int i = 0; i < b.value.Count; i++)
            {
                long theAdress = b.toAdress[i];
                long setMask = 0;
                long nollMask = 0;
                for (int variations = 0; variations < Math.Pow(2, xs); variations++)
                {
                    char[] myskomask = mask.ToArray();
                    int myskomaskBit = 0;
                    setMask = 0;
                    nollMask = 0;
                    for(int m=0; m<mask.Length; m++)
                    {
                        if (mask[m] == 'X')
                        {
                            // Magic floating stuff
                            myskomask[m] = ((1 << myskomaskBit) & variations) > 0 ? '1' : 'X';
                            myskomaskBit++;
                        }
                    }

                    for (long j = 0; j < myskomask.Length; j++)
                    {
                        // Tanken är att llra sen
                        if (myskomask[(int)j] == '1')
                            setMask = setMask | (long)((long)1 << (int)(mask.Length - j - 1));
                        else if (myskomask[(int)j] == 'X')
                            nollMask = nollMask | (long)((long)1 << (int)(mask.Length - j - 1));
                    }
                    string hej = new string(myskomask);
                
                    long tmp = theAdress | setMask;
                    tmp &= ~nollMask;
                    memory[tmp] = b.value[i];
                }
            }
        }

        List<Block> GetBlocks()
        {
            // Parse input
            char[] sepp = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(sepp, StringSplitOptions.RemoveEmptyEntries);
            List<Block> res = new List<Block>();

            // Gaaah. olika antal mem-rader
            int index = 0;
            for (int i = 0; i < sp.Length;)
            {
                Block b;
                b.toAdress = new List<long>();
                b.value = new List<long>();
                b.mask = sp[index];
                index++;
                if (index == sp.Length)
                    break;
                while (sp[index].StartsWith("mem"))
                {
                    string[] numbers = Regex.Split(sp[index], @"\D+");
                    b.toAdress.Add(int.Parse(numbers[1]));
                    b.value.Add(int.Parse(numbers[2]));
                    index++;
                    if (index == sp.Length)
                        break;
                }
                i = index;
                res.Add(b);
            }
            return res;
        }
    }
}
