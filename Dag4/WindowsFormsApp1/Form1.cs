﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int tot = 0;
            List<string> passports = GetPassaportes(); 
            foreach (string ap in passports)
                if (ValidPassaporte(ap))
                    tot++;
            Console.WriteLine("Yo: " + tot);
        }

        List<string> GetPassaportes()
        {
            char[] seppos = { '\n' };
            string[] s = textBox1.Text.Split(seppos);
            // Oh.. knäpp
            int empty = 0;
            List<string> res = new List<string>();
            string tmp = "";


            // Ny taktik.. \r finns kvar            
            foreach(string st in s)
            {
                if (st == "\r")
                {
                    // Föregående klar
                    res.Add(tmp);
                    tmp = "";
                }
                else
                {
                    if (st[st.Length-1] == '\r')
                        tmp += " " + st.Remove(st.Length - 1);
                    else
                        tmp += " " + st;
                }
            }
            
            res.Add(tmp);
            return res;
        }

        bool ValidPassaporte(string s)
        {
            string[] what = { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
            foreach(string id in what)
            {
                if (s.Contains(id) == false)
                    return false;                
            }
            char[] blanks = { ' ' };
            char[] co = { ':' };
            string[] fields = s.Split(blanks, StringSplitOptions.RemoveEmptyEntries);
            foreach(string f in fields)
            {
                switch (f.Split(co)[0])
                {
                    case "byr":
                        int byr = GetNum(f);
                        if (byr < 1920 || byr > 2002)
                            return false;
                        // Check 4 digits?
                        if (!Match(f, @":[0-9]{4}$"))
                            return false;

                        break;
                    case "iyr":
                        int iyr = GetNum(f);
                        if (iyr < 2010 || iyr > 2020)
                            return false;
                        if (!Match(f, @":[0-9]{4}$"))
                            return false;
                        break;
                    case "eyr":
                        int n = GetNum(f);
                        if (n < 2020 || n > 2030)
                            return false;
                        if (!Match(f, @":[0-9]{4}$"))
                            return false;
                        break;
                    case "hgt":
                        if (!CheckHeight(f))
                            return false;
                        break;
                    case "hcl":
                        if (!Match(f, @":#[0-9a-f]{6}$"))
                            return false;
                        break;
                    case "ecl":
                        if (!OneMatchOnly(f, new string[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }))
                            return false;
                        break;
                    case "pid":
                        // 9 digit
                        if (!Match(f, @":[0-9]{9}\b"))
                            return false;
                        break;
                }
            }
            return true;
        }

        bool CheckHeight(string s)
        {
            int n = int.Parse(Regex.Match(s, @"\d+").Value);
            if (s.Contains("cm"))
                return n >= 150 && n <= 193;
            if (s.Contains("in"))
                return n >= 59 && n <= 76;
            return false;
        }

        int GetNum(string s)
        {
            char[] seppos = { ':' };
            string[] sp = s.Split(seppos);
            return int.Parse(sp[1]);
        }

        bool OneMatchOnly(string s, string[] pattern)
        {
            int tot = 0;
            foreach(string p in pattern)
            {
                Match match = Regex.Match(s, p);
                if (!match.Success)
                    continue;
                match = match.NextMatch();
                if (!match.Success)
                    tot++;
            }
            if (tot==1)
                return true;
            return false;
        }

        bool Match(string s, string expression)
        {
            return Regex.Match(s, expression).Success;
        }
    }
}
