﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> groupos = GetGroups();
            int sum = 0;
            foreach (string s in groupos)
                sum += GroupCount2(s);

            Console.WriteLine("Yeah: " + sum);
        }

        int GroupCount2(string s)
        {
            Dictionary<char, int> dick = new Dictionary<char, int>();
            char[] seppos = { '\r' };
            string[] sp = s.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            bool first = true;
            foreach (string person in sp)
            {
                foreach (char c in person)
                {
                    if (first)
                    { 
                        dick[c] = 1;
                    }
                    else
                    {
                        // Oh boy.. hmm 
                        if (dick.ContainsKey(c))
                            dick[c]++;
                    }
                }
                first = false;
            }

            int answer = 0;
            foreach (int v in dick.Values)
            {
                if (v == sp.Length)
                    answer++;
            }
            
            return answer;
        }

        int GroupCount(string s)
        {
            Dictionary<char, bool> dick= new Dictionary<char, bool>();
            foreach(char c in s)
            {
                if (c != '\r')
                {
                    dick[c] = true;
                }
                    
            }
            int answer = 0;
            foreach (char k in dick.Keys)
                answer++;
            return answer;
        }

        List<string> GetGroups()
        {
            char[] seppos = { '\n' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);


            List<string> result = new List<string>();
            string tmp = "";            
            foreach(string s in sp)
            {
                if (s == "\r")
                {                    
                    result.Add(tmp);
                    tmp = "";
                }
                else
                {
                    tmp += s;
                }
            }

            // Do we have another one?
            if (tmp.Length > 0)
                result.Add(tmp);

            return result;
        }
    }
}
