﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag8
{
    public class instruction
    {
        public string type;
        public int offset;
        public bool didRunThisOne;
    }
    public class Computer
    {
        List<instruction> instructions;
        long accumulator;
        int pp;

        public bool Finito;
        public bool Correct;

        public Computer(List<instruction> input)
        {
            instructions = input;
        }

        public void RunNext()
        {
            if (pp == instructions.Count)
            {
                Console.WriteLine("Program terminated!! Acc = " + accumulator);
                Finito = true;
                Correct = true;
                return;
            }

            instruction next = instructions[pp];
            if (next.didRunThisOne)
            {
                Console.WriteLine("Finito: " + accumulator);
                Finito = true;
            }
            else
            {
                switch(next.type)
                {
                    case "nop":
                        pp++;
                        break;
                    case "jmp":
                        pp += next.offset;
                        break;
                    case "acc":
                        accumulator += next.offset;
                        pp++;
                        break;
                    default:
                        throw new Exception("Unknown op : " + next.type);
                }
                next.didRunThisOne = true;
            }
        }
    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<instruction> instructions= GetInput(textBox1.Text);

            foreach(instruction i in instructions)
            {
                if (i.type == "jmp")
                {
                    i.type = "nop";

                    // Testrun a computer
                    Computer c = new Computer(instructions);
                    while(c.Finito == false)
                    {
                        c.RunNext();
                    }
                    if (c.Correct)
                        Console.WriteLine("Wohoo");

                    // Switch back to jmp
                    i.type = "jmp";
                }
                else if(i.type == "nop")
                {
                    i.type = "jmp";

                    // Testrun a computer
                    Computer c = new Computer(instructions);
                    while (c.Finito == false)
                    {
                        c.RunNext();
                    }
                    if (c.Correct)
                        Console.WriteLine("Wohoo");

                    // Switch back to nop
                    i.type = "nop";
                }

                // Oh wash the instructions
                foreach (instruction wash in instructions)
                    wash.didRunThisOne = false;
            }
        }

        public List<instruction> GetInput(string input)
        {
            List<instruction> instructions = new List<instruction>();
            char[] seppos = { '\r', '\n' };
            string[] sp = input.Split(seppos, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in sp)
            {
                char[] space = { ' ' };
                string[] instr = line.Split(space, StringSplitOptions.RemoveEmptyEntries);
                instruction a = new instruction();
                a.type = instr[0];
                a.offset = int.Parse(instr[1]);
                instructions.Add(a);
            }
            return instructions;
        }
    }
}
