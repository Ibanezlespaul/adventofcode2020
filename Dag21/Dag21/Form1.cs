﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag21
{
    public class Food
    {
        public List<string> Ingredients;
        public List<string> Allergens;

        public Food(string input)
        {
            Ingredients = new List<string>();
            Allergens = new List<string>();
            char[] seppos = { ' ', ',' };
            string[] data = input.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            bool ing = true;
            foreach(string s in data)
            {
                if (s.Contains('('))
                    ing = false;

                if (ing)
                    Ingredients.Add(s);
                else
                {
                    if (s.StartsWith("("))
                        continue;//Allergens.Add(s.Substring(1));
                    else if (s.EndsWith(")"))
                        Allergens.Add(s.Substring(0, s.Length - 1));
                    else
                        Allergens.Add(s);
                }
            }
        }
    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Dick.. alergen , suspected ingredients
            Dictionary<string, List<string>> dick = new Dictionary<string, List<string>>();
            Dictionary<string, int> dickIngredientCount = new Dictionary<string, int>();

            List<Food> theFood = GetInput();
            bool changed = true;
            while (changed)
            {
                changed = false;
                foreach (Food f in theFood)
                {
                    foreach (string allergen in f.Allergens)
                    {
                        if (dick.ContainsKey(allergen))
                        {
                            // Here is the tricky part.. 
                            // Remove non common ingredients as suspected
                            foreach (string ing in f.Ingredients)
                            {
                                if (dick[allergen].Contains(ing))
                                {
                                    // Safe to remove the other ons??
                                    // No yes no yes no yes? Lets try
                                    List<string> suspects = dick[allergen].ToList();
                                    // Remove all other?? 
                                    foreach (string sus in suspects)
                                    {
                                        if (sus != ing && f.Ingredients.Contains(sus) == false)
                                        {
                                            dick[allergen].Remove(sus);
                                            changed = true;
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            // Oh.. add all ingredients as suspected!?!
                            dick[allergen] = f.Ingredients.ToList(); // Own copy right? =)
                        }
                    }
                }
            }


            // Dictionary contains ingredients with allergens... soo. which do not?
            foreach(Food f in theFood)
            {
                foreach (string ing in f.Ingredients)
                {
                    if (dickIngredientCount.ContainsKey(ing) == false)
                        dickIngredientCount[ing] = 0;

                    dickIngredientCount[ing] += 1;
                }
            }


            int sumAppear = 0;
            foreach(string k in dickIngredientCount.Keys)
            {
                bool clean = true;
                foreach(string al in dick.Keys)
                {
                    if (dick[al].Contains(k))
                        clean = false;                        
                }
                if (clean)
                {
                    sumAppear += dickIngredientCount[k];
                }
            }
            Console.WriteLine("Gogo " + sumAppear);


            // Weell.. oops.. this is a bit strange, Part 1 works like it should
            // gotta reduce dictinoary of allergens to only have one ingredient

            changed = true;
            while(changed)
            {
                changed = false;
                foreach(string k in dick.Keys)
                {
                    if (dick[k].Count == 1)
                    {
                        // Safely remove this ingredient from other allergens
                        string ing = dick[k][0];
                        foreach(string k2 in dick.Keys)
                        {
                            if (k != k2)
                            {
                                bool removed = dick[k2].Remove(ing);
                                if (removed)
                                    changed = true;
                            }
                        }
                    }
                }
            }

            // Oh.. sort stuff.. lets be smart
            List<string> allIng = new List<string>();
            foreach (string k in dick.Keys)
                allIng.Add(k + " " + dick[k][0]);

            string res = "";
            allIng.Sort();
            foreach(string s in allIng)
            {
                char[] seps = { ' ' };
                string[] sp = s.Split(seps, StringSplitOptions.RemoveEmptyEntries);
                res += sp[1] + ",";
            }

            Console.WriteLine(res);
        }

        List<Food> GetInput()
        {
            char[] sepps = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(sepps, StringSplitOptions.RemoveEmptyEntries);
            List<Food> res = new List<Food>();
            foreach(string s in sp)
            {
                res.Add(new Food(s));
            }
            return res;
        }

        
    }
}
