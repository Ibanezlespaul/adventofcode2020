﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag17
{
    public class Cube
    {

    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[,,,] myGrid = GetGrid();

            int cycles = 6;
            for (int i = 0; i < cycles; i++)
            {
                Console.WriteLine("Cycle # " + i + 1);
                //PrintGrid(myGrid);
                ApplyRules(myGrid);
            }

            int act = CountActive(myGrid);
            Console.WriteLine("Yea boy " + act);
            MessageBox.Show("Yeah " + act);
        }

        void PrintGrid(char[,,] gridden)
        {
            for (int z = 0; z < gridden.GetLength(2); z++)
            {
                for (int y = 0; y < gridden.GetLength(1); y++)
                {
                    for (int x = 0; x < gridden.GetLength(0); x++)
                    {
                        if (z==50)
                            Console.Write(gridden[x, y,z]);
                    }
                    if (z == 50)
                       Console.WriteLine("");
                }
                Console.WriteLine("ZZZZ");
            }
        }

        bool ApplyRules(char[,,,] grid)
        {
            // Did we change anything?
            char[,,,] before = grid.Clone() as char[,,,];
            bool changed = false;
            for (int w = 0; w < before.GetLength(3); w++)
                for (int z = 0; z < before.GetLength(2); z++)
            {
                for (int y = 0; y < before.GetLength(1); y++)
                {
                    for (int x = 0; x < before.GetLength(0); x++)
                    {
                        // Rule 1 part 1
                        // If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active. Otherwise, the cube becomes inactive.
                        if (before[x, y, z, w] == '#')
                        {
                            int occadj = GetOccAdjPart1(before, x, y, z, w);
                            if (occadj == 2 || occadj == 3)
                            {
                                // Remain active                                
                                
                            }
                            else
                            {
                                // Go inactive
                                grid[x, y, z, w] = '.';
                                changed = true;
                            }
                        }
                        else
                        {

                            // Rule 2 part 1
                            // If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active. Otherwise, the cube remains inactive.

                            int occadj = GetOccAdjPart1(before, x, y, z, w);
                            if (occadj == 3)
                            {
                                // Go active
                                grid[x, y, z, w] = '#';
                                changed = true;
                            }
                            else
                            {
                                // Remain inactive
                            }                            
                        }
                    }
                }
            }
            return changed;
        }

        int GetOccAdjPart1(char[,,,] grid, int ix, int iy, int iz, int iw)
        {
            // Oj oj pass på
            int sum = 0;
            int maxX = grid.GetLength(0);
            int maxY = grid.GetLength(1);
            int maxZ = grid.GetLength(2);
            int maxW = grid.GetLength(3);

            for (int w = iw - 1; w <= iw + 1; w++)
                for (int z = iz - 1; z <= iz + 1; z++)
                    for (int y = iy - 1; y <= iy + 1; y++)
                        for (int x = ix - 1; x <= ix + 1; x++)
                        {
                            if (!(x == ix && y == iy && z == iz && w == iw))
                            {
                                if (x >= 0 && x < maxX && y >= 0 && y < maxY && z >= 0 && z < maxZ && w>=0 && w<maxW)
                                    sum += grid[x, y, z, w] == '#' ? 1 : 0;
                            }
                        }
            return sum;
        }

        int CountActive(char[,,,] grid)
        {
            int sum = 0;

            for (int w = 0; w < grid.GetLength(3); w++)
                for (int z = 0; z < grid.GetLength(2); z++)
                    for (int y = 0; y < grid.GetLength(1); y++)
                        for (int x = 0; x < grid.GetLength(0); x++)
                            sum += grid[x, y, z, w] == '#' ? 1 : 0;

            return sum;
        }

        char[,,,] GetGrid()
        {
            int size = 50;
            int zoffset = size / 2;
            char[,,,] res;
            char[] seppos = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);

            // Let's make it 100 by 100 by 100 first?!

            int theY = sp.Length;
            int theX = sp[0].Length;

            res = new char[size, size, size, size];
            for (int y = 0; y < sp.Length; y++)
            {
                for (int x = 0; x < sp[y].Length; x++)
                {
                    res[x+zoffset-5, y+zoffset-5, zoffset, zoffset] = sp[y][x];
                }
            }

            return res;
        }
    }
}
