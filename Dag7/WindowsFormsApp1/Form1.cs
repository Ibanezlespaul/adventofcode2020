﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Bag
    {
        public string bagType;
        public List<Bag> children = new List<Bag>();
        public int count = 1;
        public Bag(string type)
        {
            bagType = type;
        }

        public void AddChild(Bag c)
        {
            // Dubbelkolla att den inte finns?
            foreach (Bag n in children)
                if (n.bagType == c.bagType)
                {
                    n.count++;
                    return;
                }
            children.Add(c);
        }

        public Bag FindNode(Bag c)
        {
            if (c.bagType == bagType)
                return this;
            else
            {
                foreach(Bag n in children)
                {
                    Bag theOne = n.FindNode(c);
                    if (theOne != null)
                        return theOne;
                }
            }

            // Nooops
            return null;
        }

        //public void TagGolden()
        //{
        //    if (this.bagType == "wavy gray")
        //        Console.WriteLine("Debug");
        //    foreach(Node n in children)
        //    {
        //        if (n.bagType == "shiny gold" || n.HasGoldenChild)
        //        {
        //            // Oh.. we have a golden child. don't need to call TagGolden on that oné
        //            this.HasGoldenChild = true;
        //        }
        //        else
        //        {
        //            n.TagGolden();
        //            if (n.HasGoldenChild)
        //                this.HasGoldenChild = true;
        //        }
        //    }
        //}


        //public int HaveChildOf(string bagtype, bool topLevel)
        //{
        //    int sum = 0;
        //    bool direktBarn = false;
        //    bool yeah = false;
        //    foreach (Node n in children)
        //    {
        //        // Om barnet ääär av bagtype
        //        if (n.bagType == bagtype)
        //        {
        //            sum=1;
        //            direktBarn = true;
        //        }
        //        else
        //        {
        //            sum += n.HaveChildOf(bagtype, false);
        //            yeah = true;
        //        }
        //    }

        //    if (yeah)
        //        sum++;
        //    return sum;
        //}

        public Bag(bool strange, string s)
        {
            char[] sepp = { ' ' };
            string[] sp = s.Split(sepp, StringSplitOptions.RemoveEmptyEntries);
            count = int.Parse(sp[0]);
            bagType = sp[1] + " " + sp[2];
        }
    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> rules = GetRules();
            List<Bag> myBags = new List<Bag>();

            // Ohh. klurigt
            foreach (string r in rules)
            {
                int i = r.IndexOf("bags");
                string bagType = r.Substring(0, i - 1);

                List<Bag> childBags = new List<Bag>();
                if (r.Contains("contain no other bags."))
                {
                    // uhu
                }
                else
                {
                    string ap = r.Substring(i);
                    int i2 = ap.IndexOf("contain ");
                    ap = ap.Substring(i2 + 8);
                    char[] sepc = { ',' };
                    string[] sp = ap.Split(sepc, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string s in sp)
                    {

                        int i3 = s.IndexOf("bag");
                        string tmp = s.Substring(0, i3);
                        if (tmp[0] == ' ')
                            tmp = tmp.Substring(1);
                        tmp = tmp.TrimEnd(' ');

                        // Ugh.. first is the count
                        Bag child = new Bag(true, tmp);
                        childBags.Add(child);

                    }
                }

                Bag aBag = new Bag(bagType);
                if (childBags.Count > 0)
                    foreach (Bag b in childBags)
                        aBag.AddChild(b);

                myBags.Add(aBag);
            }

            string shinyGold = "shiny gold";


            // Dick for bags with goldbags
            Dictionary<string, bool> dick = new Dictionary<string, bool>();

            bool didSomething = true;
            while (didSomething)
            {
                didSomething = false;
                foreach (Bag b in myBags)
                {
                    foreach (Bag child in b.children)
                    {
                        if (child.bagType == shinyGold)
                        {
                            // Wow.. the daddy is a cool one
                            if (dick.ContainsKey(b.bagType) == false)
                            {
                                dick[b.bagType] = true;
                                didSomething = true;
                            }
                        }
                        else if (dick.ContainsKey(child.bagType))
                        {
                            // Oh.. Add more
                            if (dick.ContainsKey(b.bagType) == false)
                            {
                                dick[b.bagType] = true;
                                //dick.Add(b.bagType, true);
                                didSomething = true;
                            }
                        }
                    }
                }
            }


            // NOw. match outer bags against dick to count golden outer bags
            int count = 0;
            foreach(Bag b in myBags)
            {
                if (dick.ContainsKey(b.bagType))
                    count++;
            }


            // Part 2
            count = GetBagsInGolden(myBags);

            Console.WriteLine("Godday " + (count-1));
        }

        int GetBagsInGolden(List<Bag> theBags)
        {
            // First of.. find the golden bugger
            foreach(Bag b in theBags)
            {
                if (b.bagType == "shiny gold")
                {
                    // Got it..
                    return GetBagCount(b, theBags);
                    break;
                }
            }
            return 0;
        }


        int GetBagCount(Bag b, List<Bag> theBags)
        {
            // Including children
            if (b.children.Count == 0)
                return b.count;
            else
            {
                List<int> sums = new List<int>();
                foreach(Bag c in b.children)
                {
                    sums.Add(c.count);
                }

                // Oh..  If a child is a top-node, include it
                int childIndex = 0;
                foreach(Bag c in b.children)
                {
                    foreach(Bag top in theBags)
                    {
                        if (c.bagType == top.bagType)
                        {
                            // Multiply
                            sums[childIndex] *= GetBagCount(top, theBags);
                        }
                    }
                    childIndex++;
                }

                // 
                return 1+sums.Sum();
            }
        }



        private List<string> GetRules()
        {
            char[] seppos = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            return sp.ToList();
        }

    }
}
