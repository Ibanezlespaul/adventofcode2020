﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag16
{
    public class Rule
    {
        public string name;
        int low, high, low2, high2;
        public int fieldIndex;
        public Rule(string parseit)
        {
            name = parseit.Substring(0, parseit.IndexOf(':'));
            string rest = parseit.Substring(parseit.IndexOf(':')+1);

            char[] sepp = { ' ','-' };
            string[] sp = rest.Split(sepp, StringSplitOptions.RemoveEmptyEntries);

            low = int.Parse(sp[0]);
            high = int.Parse(sp[1]);
            low2 = int.Parse(sp[3]);
            high2 = int.Parse(sp[4]);

        }

        public bool OKidok(int value)
        {
            if (value >= low && value <= high)
                return true;
            if (value >= low2 && value <= high2)
                return true;
            return false;
        }
    }


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            // Number of times we have the bad value
            Dictionary<int, int> safeorNot = new Dictionary<int, int>();
            List<Rule> rules = new List<Rule>();
            List<List<int>> tickets = GetTicketsUndRules(ref rules);
            List<List<int>> validTickets = new List<List<int>>();
            foreach (List<int> ticket in tickets)
            {
                // Check values in ticket
                bool goodTicket = true;
                foreach (int v in ticket)
                {
                    // Oh. go trough rules if not in dick
                    if (safeorNot.ContainsKey(v) == false)
                    {
                        bool oneOk = false;
                        foreach (Rule r in rules)
                        {
                            
                            // Put put pet 
                            if (r.OKidok(v))
                            {
                                oneOk = true;
                                break;
                            }
                        }
                        safeorNot.Add(v, oneOk ? 0:1);
                        if (!oneOk)
                            goodTicket = false;
                    }
                    else
                    {
                        // Om värdet 0, ok.. räkna ej upp värde
                        if (safeorNot[v] > 0)
                        {
                            safeorNot[v]++;
                            goodTicket = false;
                        }
                    }
                }
                if (goodTicket)
                    validTickets.Add(ticket);

            }


            // Part 2

            List<Rule> bestOrder = new List<Rule>();
            List<int> takenIndexes = new List<int>();
            while (rules.Count > 0)
            {
                foreach (Rule r in rules)
                {
                    // Check which indexes matches this rule
                    List<int> okIndexes = new List<int>();

                    for (int i = 0; i < validTickets[0].Count; i++)
                    {
                        // Already checked?
                        if (takenIndexes.Contains(i))
                            continue;

                        bool bad = false;
                        foreach (List<int> ticket in validTickets)
                        {
                            if (r.OKidok(ticket[i]) == false)
                            {
                                bad = true;
                                break;
                            }
                        }
                        if (!bad)
                            okIndexes.Add(i);
                    }

                    // This is very interesting if we have one ok index.., 
                    if (okIndexes.Count == 1)
                    {
                        r.fieldIndex = okIndexes[0];
                        bestOrder.Add(r);
                        rules.Remove(r);
                        takenIndexes.Add(okIndexes[0]);
                        break;
                    }
                }
            }



            // Work out the departureRules and use the index to multiply cool values
            long multo = 1;
            foreach(Rule r in bestOrder)
            {
                if (r.name.StartsWith("departure"))
                {
                    multo *= tickets[0][r.fieldIndex];
                }
            }
            Console.WriteLine("Frutto " + multo);



            // Add bad ones
            int sum = 0;
            foreach(int key in safeorNot.Keys)
            {                
                    sum += key*safeorNot[key];
            }
            Console.WriteLine("Tjo : " + sum);



        }

        List<List<int>> GetTicketsUndRules(ref List<Rule> theRules)
        {
            char[] seppos = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            List<List<int>> tickets = new List<List<int>>();
            bool rules = true;
            foreach(string s in sp)
            {
                if (s.StartsWith("your ticket"))
                    rules = false;

                if (rules)
                {
                    Rule r = new Rule(s);
                    theRules.Add(r);
                }
                else
                {
                    // Oh.. might be a ticket row this one
                    if (!s.StartsWith("your") && !s.StartsWith("nearby"))
                    {
                        char[] seps = { ',' };
                        List<int> tickints = new List<int>();
                        foreach(string anint in s.Split(seps, StringSplitOptions.RemoveEmptyEntries))
                        {
                            tickints.Add(int.Parse(anint));
                        }
                        tickets.Add(tickints);
                    }
                }
            }
            return tickets;
        }
    }
}
