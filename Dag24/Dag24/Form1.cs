﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Dag24
{
    public class HexCell
    {
        public char Color = 'W';
        private int x, y, z;

        public HexCell(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Use cube coordinates??
            int blackTiles = 0;
            int size = 200;
            int days = 100;
            int off = size / 2;
            char[,,] grid = new char[size, size, size];


            // Init grid to whiiite
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    for (int k = 0; k < size; k++)
                        grid[i, j, k] = 'W';

            List<List<string>> tiles = GetInput();

            // Gah.. must first do part 1 right?
            foreach (List<string> lst in tiles)
            {
                int x, y, z;
                x = y = z = off;
                foreach (string dir in lst)
                {
                    switch (dir)
                    {
                        case "e":
                            x += 1;
                            y -= 1;
                            break;
                        case "w":
                            x -= 1;
                            y += 1;
                            break;
                        case "ne":
                            x += 1;
                            z -= 1;
                            break;
                        case "sw":
                            x -= 1;
                            z += 1;
                            break;
                        case "nw":
                            z -= 1;
                            y += 1;
                            break;
                        case "se":
                            z += 1;
                            y -= 1;
                            break;
                    }
                }
                // Part 1 rules
                // Where did we end up? flip this suckerpunch
                if (grid[x, y, z] == 'W')
                {
                    grid[x, y, z] = 'B';
                    blackTiles++;
                }
                else
                {
                    grid[x, y, z] = 'W';
                    blackTiles--;
                }
            }

            // Part 2
            for (int d = 0; d < days; d++)
            {
                Console.WriteLine("Dag " + d);
                char[,,] orig = (char[,,])grid.Clone();


                // Oh.. smart här
                for (int x = 1; x < size-1; x++)
                {
                    for (int y = 1; y < size-1; y++)
                    {
                        for (int z = 1; z < size-1; z++)
                        {
                            int xx = x - off;
                            int yy = y - off;
                            int zz = z - off;
                            // Valid coordinate?

                            if (!(xx + yy + zz == 0))
                                continue;

                            // Part 2 is a bit more involved
                            int blacks = GetBlacks(xx+off, yy + off, zz + off, orig);
                            if (orig[xx+off, yy+off, zz+off] == 'B' && (blacks > 2 || blacks == 0))
                            {
                                grid[xx + off, yy + off, zz + off] = 'W';
                                blackTiles--;
                            }


                            if (orig[xx + off, yy + off, zz + off] == 'W' && blacks == 2)
                            {
                                grid[xx + off, yy + off, zz + off] = 'B';
                                blackTiles++;
                            }

                        }
                    }
                }



                // Part 1 rules
                // Where did we end up? flip this suckerpunch
                //if (grid[x, y, z] == 'W')
                //{
                //    grid[x, y, z] = 'B';
                //    blackTiles++;
                //}
                //else
                //{
                //    grid[x, y, z] = 'W';
                //    blackTiles--;
                //}


                Console.WriteLine(blackTiles);
            }
            

            

            Console.WriteLine("Black ones " + blackTiles);
        }

        int GetBlacks(int x, int y, int z, char[,,] grid)
        {
            int blackSum = 0;
            List<List<int>> directions = new List<List<int>>() { new List<int>{ 1, -1, 0 }, new List<int> { 1, 0, -1 }, new List<int> { 0, 1, -1 }, new List<int> { -1, +1, 0 }, new List<int> { -1, 0, 1 }, new List<int> { 0, -1, +1 } };
            foreach(List<int> dir in directions)
            {
                if (grid[x+dir[0], y+dir[1], z+dir[2]] == 'B')
                    blackSum++;
            }
            return blackSum;
        }


        List<List<string>> GetInput()
        {
            char[] sepps = { '\r', '\n' };
            string[] sp = textBox1.Text.Split(sepps, StringSplitOptions.RemoveEmptyEntries);
            List<List<string>> res = new List<List<string>>();

            foreach(string s in sp)
            {
                List<string> tmp = new List<string>();

                // Split up into moves
                for (int p = 0; p < s.Length; p++)
                {
                    switch (s[p])
                    {
                        case 'e':
                            tmp.Add("e");
                            break;
                        case 'w':
                            tmp.Add("w");
                            break;
                        case 'n':
                            if (s[p + 1] == 'e')
                                tmp.Add("ne");
                            else
                                tmp.Add("nw");
                            p++;
                            break;
                        case 's':
                            if (s[p + 1] == 'e')
                                tmp.Add("se");
                            else
                                tmp.Add("sw");
                            p++;
                            break;
                    }
                }
                res.Add(tmp);                                
            }

            return res;
        }
    }
}
