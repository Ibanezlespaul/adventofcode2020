﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Get tha map
            List<string> mappis = GetRows();
            int[] xstep = { 1, 3, 5, 7, 1 };
            int[] ystep = { 1, 1, 1, 1, 2 };
            List<int> results = new List<int>();

            for (int t = 0; t < xstep.Length; t++)
            {
                int treees = 0;
                int x = 0;
                int y = 0;
                do
                {
                    x += xstep[t];
                    y += ystep[t];
                    if (mappis[y][x] == '#')
                        treees++;

                } while (y + ystep[t] < mappis.Count);

                Console.WriteLine("Bunga: " + treees);
                results.Add(treees);
            }

            // Multiflix
            long mulle = results[0];
            for(int i=1; i<results.Count; i++)
            {
                mulle *= results[i];
            }
            Console.WriteLine("Ahh: " + mulle);
        }

        List<string> GetRows()
        {
            char[] seppos = { '\r', '\n' };
            string[] s = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            // OH.. make it wider plzzz.. may be importante
            List<string> res = new List<string>();
            foreach(string st in s)
            {
                string bilt = st;
                for (int i = 0; i < 80; i++)
                    bilt += st;
                res.Add(bilt);
            }
            return res;
        }
    }
}
