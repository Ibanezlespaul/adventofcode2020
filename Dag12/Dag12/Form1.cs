﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int dir;
        long x, y;
        long wx, wy;
        private void button1_Click(object sender, EventArgs e)
        {
            List<string> input = GetInput();
            dir = 0;

            x = 0; y = 0;
            wx = 10; wy = 1;
            foreach(string s in input)
            {
                int v = int.Parse(s.Substring(1));
                switch(s[0])
                {
                    case 'N':
                        wy += v;
                        break;
                    case 'E':
                        wx += v;
                        break;
                    case 'S':
                        wy -= v;
                        break;
                    case 'W':
                        wx -= v;
                        break;
                    case 'R':
                        Rotate(-v);
                        //dir += v;
                        break;
                    case 'L':
                        Rotate(v);
                        //dir -= v;
                        break;
                    case 'F':
                        x += wx * v;
                        y += wy * v;
                        //x += (int)Math.Cos(dir * Math.PI / 180.0) * v;
                        //y -= (int)Math.Sin(dir * Math.PI / 180.0) * v;
                        break;                        
                }
                Console.WriteLine(s + " moved to " + x + " " + y + " Waypoint " + wx + " " + wy);
                

            }
            Console.WriteLine("Manhattan " + (Math.Abs(x) + Math.Abs(y)));


        }

        void Rotate(int deg)
        {
            // Rotate waypoint
            double rad = Math.PI * deg / 180.0;

            long tmpx = wx;
            long tmpy = wy;

            wx = (long)Math.Round(Math.Cos(rad) * tmpx - Math.Sin(rad) * tmpy);
            wy = (long)Math.Round(Math.Sin(rad) * tmpx + Math.Cos(rad) * tmpy);
        }

        List<string> GetInput()
        {
            char[] sepp = { '\n', '\r' };
            return textBox1.Text.Split(sepp, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
