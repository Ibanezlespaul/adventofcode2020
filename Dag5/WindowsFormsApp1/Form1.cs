﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int high = -1;
            List<int> taken = new List<int>();
            foreach (string b in GetBoardingPasses())
            {
                int id = GetID(b);
                taken.Add(id);
                high = Math.Max(id, high);
            }
            taken.Sort();

            foreach (int oh in taken)
            {
                Console.WriteLine("Yeah: " + oh);
            }
        }

        int GetID(string bp)
        {
            int masken = 0;
            for(int i=0; i<7; i++)
            {
                // Keep front 
                if (bp[i] == 'F')
                    masken = masken << 1;
                else
                {
                    masken = (masken << 1) ^ 1;
                }
            }
            //Console.WriteLine("Row: " + masken);
            int mask2 = 0;
            for (int i=0;i<3;i++)
            {
                if (bp[i + 7] == 'L')
                    mask2 = mask2 << 1;
                else
                    mask2 = (mask2 << 1) ^ 1;
            }
            //Console.WriteLine("Column: " + mask2);
            return masken * 8 + mask2;
        }


        List<string> GetBoardingPasses()
        {
            char[] seppos = { '\r', '\n' };
            return textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
