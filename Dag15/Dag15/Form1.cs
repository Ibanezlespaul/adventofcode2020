﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<long> memory = GetInput();
            long last = memory.Last();
            memory.RemoveAt(memory.Count - 1);

            Dictionary<long, long> dick = new Dictionary<long, long>();
            // Start
            for (int i=0; i<memory.Count; i++)
            {
                if (dick.ContainsKey(memory[i]))
                    dick[memory[i]] = i;
                else
                    dick[memory[i]] = i;

            }

            // Do some smart stuff
            long counter = memory.Count;
            while(true)
            {
                long next = dick.ContainsKey(last) ? counter - dick[last] : 0;
                dick[last] = counter;
                last = next;
                counter++;
                if (counter == 30000000-1)
                {
                    Console.WriteLine("Cowabung : " + last);
                    return;
                }
            }

            //            while (true)
            //            {
            //                long index = memory.LastIndexOf(last);
            //                memory.Add(last);
            //                if (index == -1)
            //                    last = 0;
            //                else
            //                    last = counter-index;
            ////                long next = LastIndex(memory);
            //                counter++;
            //                if (counter == 30000000)
            //                {
            //                    Console.WriteLine("Cowabung : " + memory.Last());
            //                    return;
            //                }
            //            }

        }

        long LastIndex(List<long> data)
        {
            long x = data.Last();
            for (long i = data.Count - 2; i >= 0; i--)
                if (data[(int)i] == x)
                    return data.Count - i-1;
            return 0;
        }
        List<long> GetInput()
        {
            char[] seps = { ',' };
            string[] sp = textBox1.Text.Split(seps, StringSplitOptions.RemoveEmptyEntries);
            List<long> res = new List<long>();
            foreach (string s in sp)
                res.Add(long.Parse(s));
            return res;
        }
    }
}
