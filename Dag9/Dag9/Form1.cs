﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dag9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] seppos = { '\n', '\r' };
            string[] sp = textBox1.Text.Split(seppos, StringSplitOptions.RemoveEmptyEntries);
            List<long> values = new List<long>();
            foreach(string s in sp)
            {
                values.Add(long.Parse(s));
            }

            //FindFirst(values);
            FindSet(values);
        }

        void FindFirst(List<long> numbers)
        {
            int preamble = 25;
            
            for(int i=preamble; i<numbers.Count; i++)
            {
                long check = numbers[i];
                bool foundSum = FindSum(preamble, check, i, numbers);
                if (foundSum == false)
                    Console.WriteLine("Troublesome " + check);
            }
        }

        bool FindSum(int preamble, long value, int index, List<long> numbers)
        {
            for (int i=index-preamble; i<index; i++)
                for (int j = index - preamble; j < index; j++)
                {
                    if (i != j && numbers[i] + numbers[j] == value)
                        return true;
                }
            return false;
        }

        bool FindSet(List<long> numbers)
        {
            long theSum = 177777905;
            int setLength = 2;
            bool found = false;
            while (true)
            {
                for (int i = 0; i < numbers.Count - setLength; i++)
                {
                    long sum = 0;
                    List<long> settish = new List<long>();
                    for (int j=i; j < i+setLength; j++)
                    {
                        sum += numbers[j];
                        settish.Add(numbers[j]);
                    }
                    if (sum == theSum)
                    {
                        // Wow.. find largets and smallest value in this list =)
                        Console.WriteLine("Small " + settish.Min() + " max " + settish.Max());
                        Console.WriteLine("Added: " + (settish.Min() + settish.Max()));
                        return true;
                    }
                }
                setLength++;
            }
            return false;
        }
    }
}
